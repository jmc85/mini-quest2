﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    public Button playButton, controlsButton, backButton;
    public Image controlsImage;

    public void Back()
    {
        playButton.gameObject.SetActive(true);
        controlsButton.gameObject.SetActive(true);
        backButton.gameObject.SetActive(false);
        controlsImage.gameObject.SetActive(false);
    }
}
