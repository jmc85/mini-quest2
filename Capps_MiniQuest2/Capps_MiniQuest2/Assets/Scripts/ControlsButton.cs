﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsButton : MonoBehaviour
{
    public Button playButton, controlsButton, backButton;
    public Image controlsImage;

    public void ShowControls()
    {
        playButton.gameObject.SetActive(false);
        controlsButton.gameObject.SetActive(false);
        backButton.gameObject.SetActive(true);
        controlsImage.gameObject.SetActive(true);
    }
}
