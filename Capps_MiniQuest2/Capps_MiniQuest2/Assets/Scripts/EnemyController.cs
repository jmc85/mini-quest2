﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{ 
    public LayerMask ground;

    private int direction = 1;
    private SpriteRenderer rend;

    private void Start()
    {
         rend = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
         RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1f, ground);

         if (hit.collider == null)
         {
              direction *= -1;
              rend.flipX = !rend.flipX;
         }
         transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x + 1 * direction, transform.position.y), Time.fixedDeltaTime);
    }
}
