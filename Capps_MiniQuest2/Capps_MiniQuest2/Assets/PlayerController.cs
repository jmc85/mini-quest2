﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D body;
    private SpriteRenderer rend;
    private Animator anim; 
    private float h;
    private bool jump = false;
    private bool isGrounded = true;
    private float fireRate = 0.3f; //added
    private float nextFire = 0; //added
    private bool isFacingRight = true; //added
    private int score = 0, numCrystals = 0, health = 5;
    private AudioSource audioSource;
   
    public Image healthContainer;
    public Sprite[] healthImages;
    public GameObject screen;
    public AudioClip shootSound, jumpSound, coinSound, winSound;
    public Text scoreText, goal, numOfCrystals;
    public float moveForce = 150f;
    public float maxSpeed = 5f;
    public float jumpForce = 400f; 
    public Transform groundCheck;
    public GameObject fire;
    public Transform firePosition;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        rend = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        StartCoroutine(instruction());
    }

    void Update()
    {
        scoreText.text = "Collected Crystals: " + score;
        numOfCrystals.text = "Total Crystals Collected | " + numCrystals + " out of 53";

        h = Input.GetAxis("Horizontal");

        // make player stop quicker
        if (Mathf.Abs(h) < 0.5)
            h = 0;

        // make sprite face correct direction
        if (h > 0 && !isFacingRight) //moving to right
            flip(); //updated
        else if (h < 0  && isFacingRight) //moving to left
            flip(); //updated

        Debug.DrawRay(groundCheck.position, new Vector2(0, 0.2f), Color.red, 1f);
        RaycastHit2D grounded = Physics2D.Raycast(groundCheck.position, Vector2.down, 0.2f); //0.2f is distance

        if (grounded.collider != null)  //Player has landed
        {
            isGrounded = true;
            anim.SetBool("IsJumping", false);  
        }
        else
        {
            isGrounded = false;
            anim.SetBool("IsJumping", true);
        }

        // set player anim
        if (h != 0 && isGrounded)
            anim.SetBool("IsWalking", true);
        else
            anim.SetBool("IsWalking", false);

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            audioSource.PlayOneShot(jumpSound);
            jump = true;
        }
        if (Input.GetButton("Fire1") && Time.time > nextFire)//added
        {
            audioSource.PlayOneShot(shootSound);
            nextFire = Time.time + fireRate;
            Instantiate(fire, firePosition);
        }
    }

    void FixedUpdate()
    {
        body.AddForce(Vector2.right * h * moveForce);

        // but limit how fast the player can move
        if (Mathf.Abs(body.velocity.x) > maxSpeed)
            body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * maxSpeed, body.velocity.y);

        if (jump) 
        {
            body.AddForce(Vector2.up * jumpForce);
            jump = false;
        }

        // make player fall faster
        if (body.velocity.y >= 0)
            body.gravityScale = 1f;
        else
            body.gravityScale = 5f;

    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.CompareTag("Dragon") || col.gameObject.CompareTag("Frog"))
        {
            if(health <= 0)
            {
                SceneManager.LoadScene(0);
            } else 
            {
                healthContainer.sprite = healthImages[health - 1];
                health--;
            }
            
        }

        if (col.gameObject.CompareTag("Pickup"))
        {
            numCrystals += 1;
            audioSource.PlayOneShot(coinSound);
            Destroy(col.gameObject);
            score++;
        }
        
        if(col.gameObject.CompareTag("Win"))
        {
            StartCoroutine(win());
        }
    }

    public Vector2 GetDirection()
    {
        if (isFacingRight) //added if/else here
            return Vector3.right;
        else
            return Vector3.left;
    }

    public void flip() //added this entire function
    {
        isFacingRight = !isFacingRight; // don't forget to update the flag!
        Vector3 theScale = transform.localScale;
        theScale.x = theScale.x * -1; //inverts the value
        transform.localScale = theScale;
    }

    IEnumerator win()
    {
        healthContainer.gameObject.SetActive(false);
        numOfCrystals.gameObject.SetActive(true);
        screen.GetComponent<Animation>().Play("FadeAnim");
        audioSource.PlayOneShot(winSound);
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);
    }

    IEnumerator instruction()
    {
        yield return new WaitForSeconds(6);
        goal.gameObject.SetActive(false);
    }

}
